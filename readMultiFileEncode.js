var fs       = require('fs');
var Promise  = require('promise');
var promises = [];
var promisesWriteJSONData = [];
var readEachFileObj = require('./readLargeEncode.js')
var readline = require('readline');
// merge multile files to one file
var readFile = function (linkFolder, file) {
    return new Promise(function (resolve, reject) {
        var lines = [];
        var rl    = readline.createInterface({
            input: fs.createReadStream(linkFolder +'/' + file)
        });

        rl.on('line', function (line) {
            // Split line on comma and remove quotes
            var columns = line
                .replace(/"/g, '')
                .split(',');

            lines.push(columns);
        });

        rl.on('close', function () {
            // Add newlines to lines
            lines = lines.join("\n");
            resolve(lines)
        });
    });
};

var writeFile = function (data) {
    return new Promise(function (resolve, reject) {
        fs.appendFile('output.csv', data, 'utf8', function (err) {
            if (err) {
                resolve('Writing file error!');
            } else {
                reject('Writing file succeeded!');
            }
        });
    });
};

function readDataMergeOneFile (linkFolder) {
    fs.readdir(linkFolder, function (err, files) {
        for (var i = 0; i < files.length; i++) {
            promises.push(readFile(linkFolder,files[i]));
    
            if (i == (files.length - 1)) {
                var results = Promise.all(promises);
    
                results.then(function (data) {
                        console.log('readData' ,data)
                    }).catch(function (err) {
                    console.log(err)
                });
            }
    
        }
    });
}

function readAndWriteJSONFile (linkFolder){
    fs.readdir(linkFolder, function (err, files){
        for (var i = 0; i < files.length ; i++){
            promisesWriteJSONData.push(readEachFileObj.readFileM3U(linkFolder, files[i], 1));

            if (i == (files.length -1)){
                var results = Promise.all(promisesWriteJSONData);
                results.then(function (data){
                    console.log('read Result Write JSON data', data)
                }).catch(function (err){
                    console.log('read and write JSON File Error' + err)
                })
            }
        }

    })
}

module.exports = {
    readDataMergeOneFile : readDataMergeOneFile,
    readAndWriteJSONFile: readAndWriteJSONFile
}