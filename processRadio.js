// const { forEach } = require('p-iteration');
const fetch = require('node-fetch')
// function delay() {
//     return new Promise( function(resolve,reject) {
//         setTimeout(resolve, 10000);
//     });
//   }
// async function delayedLog  (item) {
//    await delay();
//     console.log(item);
// };

// async function printFiles () {
//  let files = [1,2,3,4];
//   await forEach(files, async function (item) {
//      await delayedLog(item);
//   });
// }



// printFiles();

function delay(time) {
    return new Promise( function (resolve){
        setTimeout( function () {
            fetch('http://dummy.restapiexample.com/api/v1/employees')
            .then(res => res.text())
            .then(body => {
              //  var strName = linkUrl.substring(linkUrl.lastIndexOf("https://freeiptv.io/m3u/v527m_normal.php?c="))
                return resolve(body);
            });
        },time);
    }); 
}
  
  async function delayedLog(item, time) {
    // notice that we can await a function
    // that returns a promise
   let data =  await delay(time);
   console.log("--------BEGIN----------" + time + "-----------------");
    console.log(data);
    console.log("-----------------END---------------");
  }

  
//   async function processArray(array) {
//     for (const item of array) {
//       await delayedLog(item);
//     }
//     console.log('Done!');
//   }
  async function processArray(arr) {
    for ( let i = 0; i < arr.length; i++)
    {
        await delayedLog(arr[i], (i +1)* 2000);
    }
    console.log('Done!');
  }


  
  processArray([1, 2, 3]);