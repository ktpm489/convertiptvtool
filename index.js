const cron = require("node-cron");
const express = require("express");
const fs = require("fs");
var sendMailObj = require('./sendMailReport.js')
app = express();
const host = '0.0.0.0';
const port = process.env.PORT || 3000;
// http://tuohuang.info/nodejs-cronjob-with-scheduler-on-heroku#.W_ie4h8XrIU
// https://scotch.io/tutorials/nodejs-cron-jobs-by-examples
//      * * * * * *
//      | | | | | |
//      | | | | | day of week
//      | | | | month
//      | | | day of month
//      | | hour
//      | minute
//      second ( optional )
// var task = cron.schedule("00 06 * * *", () =>{
//     // console.log("running a task every minutes");
//     sendMailObj.sendMail();
// }, {
//     start: true, 
//     timeZone: 'Asia/Ho_Chi_Minh' 
// })
// task.start();


// var task1 = cron.schedule("3 * * * *", () =>{
//      console.log("running a task every minutes");
//     // sendMailObj.sendMail();
// }, {
//     start: true, 
//     timeZone: 'Asia/Ho_Chi_Minh' 
// })
// task.start();
// var valid = cron.validate('59 23 * * *');
// console.log('valid', valid)
// // app.listen(5000);

app.get('/', function(request, response){
    console.log('Call api');
   // sendMailObj.sendMail();
    response.send("Hello");
})
app.get('/cron-job', function (req, res) {
    res.status(200).json({
        data: 'Welcome to the project-name api'
    });
});

app.listen(port, host, function() {
    console.log("Server started.......");
  });