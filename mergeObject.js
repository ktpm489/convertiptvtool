// https://stackoverflow.com/questions/7146217/merge-2-arrays-of-objects
var _ = require('underscore')
// var arr1 = [{name: "lang", value: "English"}, {name: "age", value: "18"}];
// var arr2 = [{name : "childs", value: '5'}, {name: "lang", value: "German"}];
// merge new arr2 to arr1 depend on property

// var arr1= [{"name":"Ilektra tv","createDate":"Mon Nov 19 2018 20:58:17 GMT+0700 (SE Asia Standard Time)","updateDate":"Mon Nov 19 2018 20:58:17 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://iptv.vlivanis.gr:25461/live/LIve_Iptv/HTLPvzOfD9/36.ts"},{"name":"Hlektra FM","createDate":"Mon Nov 19 2018 20:58:17 GMT+0700 (SE Asia Standard Time)","updateDate":"Mon Nov 19 2018 20:58:17 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://iptv.vlivanis.gr:25461/live/LIve_Iptv/HTLPvzOfD9/41.ts"}]
// var arr2= [{"name":"Ilektra tv","createDate":"Mon Nov 20 2018 20:58:17 GMT+0700 (SE Asia Standard Time)","updateDate":"Mon Nov 19 2018 20:58:17 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://iptv.vlivanis.gr:25461/live/LIve_Iptv/HTLPvzOfD9/44.ts"},
// {"name":"Hlektra FM","createDate":"Mon Nov 19 2018 20:58:17 GMT+0700 (SE Asia Standard Time)","updateDate":"Mon Nov 20 2018 20:58:17 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://iptv.vlivanis.gr:25461/live/LIve_Iptv/HTLPvzOfD9/50.ts"}]
function mergeByProperty(arr1, arr2, prop){
    _.each(arr2,function(arr2obj){
        var arr1obj = _.find(arr1, function(arr1obj){
            return arr1obj[prop] == arr2obj[prop];
        })
         // if the object already exist extend it with new values from arr2, 
         // otherwise just add new object to arr2
         arr1obj ? _.extend(arr1obj,arr2obj) :  arr1.push(arr2obj);
    })
}
// mergeByProperty(arr1, arr2, 'name');
// console.log(arr1)
module.exports = {
    mergeByProperty : mergeByProperty,
}