// https://social.msdn.microsoft.com/Forums/vstudio/en-US/47800a60-4461-4f8e-a8d1-751fa62c7884/aes-encrypt-in-javascript-and-decrypt-in-c?forum=csharpgeneral
// https://www.c-sharpcorner.com/uploadfile/4d9083/encrypt-in-javascript-and-decrypt-in-c-sharp-with-aes-algorithm/
var CryptoJS = require("crypto-js");

function test() {
    var salt = CryptoJS.enc.Utf8.parse("28698aadc97f3ad8");
    var iv = CryptoJS.enc.Utf8.parse("73ac39603da6e205");
      console.log('salt  '+ salt );
    console.log('iv  '+ iv );
    var key128Bits = CryptoJS.PBKDF2("Secret Passphrase", salt, { keySize: 128/8 }); 
    console.log( 'key128Bits '+ key128Bits);
    var data = [{id: 1}, {id: 2}]
    var key128Bits100Iterations = CryptoJS.PBKDF2("Secret Passphrase", salt, { keySize: 128/8 , iterations: 100 });
    console.log( 'key128Bits100Iterations '+ key128Bits100Iterations);
    var encrypted = CryptoJS.AES.encrypt(JSON.stringify(data), key128Bits100Iterations, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7  });
    console.log('encrypted   '+ encrypted  );
    
    
  //  ------------
  
      var salt = CryptoJS.enc.Utf8.parse("28698aadc97f3ad8");
    var iv = CryptoJS.enc.Utf8.parse("73ac39603da6e205");
    var encrypted = "k6jQV1fUrvv+wX35zwEaW+maWggc4iMeVdp3PWfNhik=";
    console.log('salt  '+ salt );
    console.log('iv  '+ iv );
    var key = CryptoJS.PBKDF2("Secret Passphrase", salt, { keySize: 128/8 , iterations: 100 });
    console.log( 'key '+ key);
    var decrypt = CryptoJS.AES.decrypt(encrypted, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
    var outputData = decrypt.toString(CryptoJS.enc.Utf8); 
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    console.log(decryptedData);
   
}
test();