function getAllFilesInGoogleFolder(drive,parentId , jwtClient) {
    drive.files.list({
        auth : jwtClient,
        pageSize: 10,
        q: "'" + parentId + "' in parents and trashed=false",
        fields: 'files(id,name)'
    },(err, {data}) =>{
        if (err) return console.log('The API returned an error: ' + err);
        const files = data.files;
        if (files.length){
            console.log('Files:');
            files.map((file)=> {
                console.log(`${file.name} (${file.id})`);
            })
        } else {
            console.log('No files found.');
        }
    })
}

module.exports = {
    getAllFilesInGoogleFolder : getAllFilesInGoogleFolder
}