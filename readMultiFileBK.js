var fs       = require('fs');
var Promise  = require('promise');
var promises = [];
var readline = require('readline');
// merge multile files to one file
var readFile = function (linkFolder, file) {
    return new Promise(function (resolve, reject) {
        var lines = [];
        var rl    = readline.createInterface({
            input: fs.createReadStream(linkFolder +'/' + file)
        });

        rl.on('line', function (line) {
            // Split line on comma and remove quotes
            var columns = line
                .replace(/"/g, '')
                .split(',');

            lines.push(columns);
        });

        rl.on('close', function () {
            // Add newlines to lines
            lines = lines.join("\n");
            resolve(lines)
        });
    });
};

var writeFile = function (data) {
    return new Promise(function (resolve, reject) {
        fs.appendFile('output.csv', data, 'utf8', function (err) {
            if (err) {
                resolve('Writing file error!');
            } else {
                reject('Writing file succeeded!');
            }
        });
    });
};

function readData (linkFolder) {
    fs.readdir(linkFolder, function (err, files) {
        for (var i = 0; i < files.length; i++) {
            promises.push(readFile(linkFolder,files[i]));
    
            if (i == (files.length - 1)) {
                var results = Promise.all(promises);
    
                results.then(function (data) {
                        console.log('readData' ,data)
                    }).catch(function (err) {
                    console.log(err)
                });
            }
    
        }
    });
}

module.exports = {
    readData : readData
}