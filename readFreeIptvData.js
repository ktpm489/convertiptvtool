const fetch = require('node-fetch');
const cheerio = require('cheerio');
const fs = require('fs-extra')
let countryData = [{ name: 'Normal AF',
link:
 [ 'https://freeiptv.io/m3u/3awqu_normal.php?c=AF',
   'https://freeiptv.io/m3u/8ulcx_sport.php?c=AF',
   'https://freeiptv.io/m3u/2cr2c_movie.php?c=AF',
   'https://freeiptv.io/m3u/zjni8_series.php?c=AF',
   'https://freeiptv.io/m3u/j72te_music.php?c=AF',
   'https://freeiptv.io/m3u/vpzrw_kids.php?c=AF',
   'https://freeiptv.io/m3u/vsgl7_news.php?c=AF',
   'https://freeiptv.io/m3u/8p5rd_faith.php?c=AF' ] }]

  
   async function delayProcess(item, time) {
    // notice that we can await a function
    // that returns a promise
    let currentData = ""
    for (let i = 0 ; i< item.link.length; i++){
        currentData = await delayAddFile( item.link[i],currentData,time);
    }
      console.log("--------BEGIN WRITE FILE----------" + item.name+ "-----------------");
        // console.log(data);
       await writeToFile(item.name + ".m3u", currentData);
      console.log("-----------------END WRITE FILE------" + item.name + "-----------------");
  }
  
    async function delayAddFile( link ,currentData,time = 2000) {
            let data = await delay(link,time);
            if (data.trim() === '#EXTM3U'){
                console.log("No data need add" + link)
              return currentData
              }else {
                console.log("Data need add" + link)
               return currentData += data;
              }
    }   
    
    async function writeToFile (fileName,data) {
        try {
            await fs.writeFile(fileName, data);
            console.log('Write file' + fileName + 'Sucesss');
        } catch (err) {
            console.error('Write file' + fileName + ' Error: ' + err)
        }
    }

    function delay(link, time) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                fetch(link)
                    .then(res => res.text())
                    .then(body => {
                        return resolve(body);
                    });
            }, time);
        });
    }


async function processCountryData(arr) {
    for (let i = 0 ; i< arr.length; i++){
        await delayProcess(arr[i], 3000);
    }
    console.log('Done All!');
}   

processCountryData(countryData)