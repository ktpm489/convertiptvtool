var CryptoJS = require("crypto-js");

function encryptData (keyInput, ivInput, data) {
                 var key = CryptoJS.enc.Utf8.parse('8080808080808080');
                var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
                var key128Bits100Iterations = CryptoJS.PBKDF2("Secret Passphrase", key, { keySize: 128/8 , iterations: 100 });
                var encryptedlogin = CryptoJS.AES.encrypt(data, key,
                {
                    keySize: 128 / 8,
                    iv: iv,
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                });
                return encryptedlogin.toString();
}

function decryptData (keyInput, ivInput, data) {
    var key = CryptoJS.enc.Utf8.parse(keyInput);
    var iv = CryptoJS.enc.Utf8.parse(ivInput);
 //   var key128Bits100Iterations = CryptoJS.PBKDF2("Secret Passphrase", key, { keySize: 128/8 , iterations: 100 });
    var decryptData = CryptoJS.AES.decrypt(data, key,
    {
        keySize: 128/8,
        iv: iv,
        mode : CryptoJS.mode.CBC,
        padding : CryptoJS.pad.Pkcs7
    })
    return decryptData.toString(CryptoJS.enc.Utf8);
}

function testEnDecryptData () {
    var keyInput = '8080808080808080';
    var ivInput = '8080808080808080';

    var data=[{"name":"AF 1TV ENTERTAINMENT PASHTOPERSIAN ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://38.101.217.15:1935/ilink/live1tv/playlist.m3u8"},{"name":"AF AL NABAA ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://cdg3.edge.tmed.pw/alnabaatv/abr/alnabaatv/live_1024p/chunks.m3u8"},{"name":"AF ANN ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://ns8.indexforce.com:1935/ann/ann/playlist.m3u8"},{"name":"AF APADANA TV ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://stream.aeducationaltv.org/watch/playlist.m3u8"},{"name":"AF DRAMA OPT 2 ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://www.elahmad.com/tv/m3u8/syriatv.m3u8"},{"name":"AF DREAMLAND TV ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://iriptv.com:1935/iriptv/dreamlandtv.stream/dreamlandtv.m3u8"},{"name":"AF KTV 1 OPT 1 ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"https://svs.itworkscdn.net/ktv1live/ktv1.smil/playlist.m3u8"},{"name":"AF KTV 1 OPT 2 ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://www.elahmad.com/tv/m3u8/ktv.m3u8"},{"name":"AF KTV 2 OPT 2 ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"https://svs.itworkscdn.net/ktv2live/ktv2.smil/playlist.m3u8"},{"name":"AF KTV AL MAJLIS ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"https://svs.itworkscdn.net/ktvalmajlislive/kalmajlis.smil/playlist.m3u8"},{"name":"AF KTV ETHRAA ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"https://svs.itworkscdn.net/ktvethraalive/kethraa.smil/playlist.m3u8"},{"name":"AF KTV PLUS ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"https://svs.itworkscdn.net/ktvpluslive/kplus.smil/playlist.m3u8"},{"name":"AF NABD ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://www.elahmad.com/tv/m3u8/akamaihd.m3u8"},{"name":"AF OMID E OITN ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://66.160.192.250:8080/oitn.tvboxhd.com/high/798536241/tracks-v1a1/index.m3u8"},{"name":"AF ONE TV ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://82.212.74.2:8000/live/7308.m3u8"},{"name":"AF OUEST TV ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://185.132.179.43:8000/live/OG82YHlMmyo4e2T/HAlIrAIpqdfhEuH/881.ts"},{"name":"AF OUEST TV 1 ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://185.132.179.43:8000/live/OG82YHlMmyo4e2T/HAlIrAIpqdfhEuH/881.m3u8"},{"name":"AF PERSIAN BAZAAR TV ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://stream.persiantv1.com/live/pbtv/index.m3u8"},{"name":"AF S CHANNEL OPT 1 ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://starmena.ercdn.net/libyaschannel/libyaschannel_480p.m3u8"},{"name":"AF S CHANNEL OPT 2 ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://1599780955@starmena.ercdn.net/libyaschannel/libyaschannel.m3u8"},{"name":"AF TANASUH TV ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://185.132.179.43:8000/live/OG82YHlMmyo4e2T/HAlIrAIpqdfhEuH/883.m3u8"},{"name":"AF TELESUD TV ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://185.132.179.43:8000/live/OG82YHlMmyo4e2T/HAlIrAIpqdfhEuH/885.ts"},{"name":"AF TELESUD TV 1 ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://185.132.179.43:8000/live/OG82YHlMmyo4e2T/HAlIrAIpqdfhEuH/885.m3u8"},{"name":"AF TVC ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://185.132.179.43:8000/live/OG82YHlMmyo4e2T/HAlIrAIpqdfhEuH/886.ts"},{"name":"AF TVC 1 ","createDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","updateDate":"Sat Jan 05 2019 11:50:57 GMT+0700 (SE Asia Standard Time)","isLive":true,"link":"http://185.132.179.43:8000/live/OG82YHlMmyo4e2T/HAlIrAIpqdfhEuH/886.m3u8"}];
    var encryptOutput = encryptData(keyInput, ivInput,  JSON.stringify(data));
    console.log('encryptOutput',encryptOutput);
    var decryptOutput = decryptData(keyInput, ivInput, encryptOutput);
    console.log('decryptOutput',decryptOutput)
}

module.exports = {
    encryptData : encryptData,
    decryptData: decryptData
}