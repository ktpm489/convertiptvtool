var fs = require("fs")
var path = require("path");
function uploadGoogleDriveFile(folderId, fileName,drive, jwtClient ){
var fileMetadata = {
    'name': fileName,
    parents: [folderId]
};
var media = {
    mimeType: 'text/plain',
    body : fs.createReadStream(path.join(__dirname, './'+ fileName))
};
drive.files.create({
 auth : jwtClient,
 resource : fileMetadata,
 media: media,
 fields : 'id'
}, function(err, file){
    if (err){
        console.log(err)
    } else {
        console.log('File Id:', file.id);
    }
})
}

module.exports = {
    uploadGoogleDriveFile: uploadGoogleDriveFile
}