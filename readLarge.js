var fs  = require('fs');
var readline = require('readline');
var path = require("path");
var stream = require('stream');
var dateFormat = require('dateformat');
var Promise  = require('promise');
let processLine = []


 // 1 -> NORMAL, 2-> Read from http://www.iptvtools.net/#
function readFileM3U(linkFolder, linkFile , type = 1) {
    return new Promise(function(resolve, reject){
        try {
            var instream = fs.createReadStream(linkFolder + '/' + linkFile);
        var outstream = new stream;
        var r1 = readline.createInterface(instream, outstream);
        let arr  = [];
        let obj = {};
        let currentType = linkFile.indexOf('custom') > -1 ? 2: type
        r1.on('line', function(line){
           handleProcessLine(line,arr, obj,currentType)
        })
       
        r1.on('close', function(){
            try {
                // Promise.all(processLine).then(function(data) {
                  console.log('arr',arr)
                    var myJsonString = JSON.stringify(arr);
                    // console.log('close', 'myJsonString', myJsonString)
                    var extension = path.extname(linkFile);
                    var file = path.basename(linkFile,extension)+ '_'+ dateFormat(new Date(), "dd_mm_yyyy_HH_MM_ss_TT") +  path.extname(linkFile);
                    console.log('file', file )
                    fs.writeFile(linkFolder + '/' + file, myJsonString,function(err){
                        if (err){
                        console.log('err',err);
                        resolve('write ' + linkFile + 'error')
                        }
                        resolve('write ' + linkFile + 'OK')
                        console.log('save file OK');
                    } )
                // })
                
            } catch (error){
                console.log('error',error)
                resolve('write ' + linkFile + 'error')
            }finally {
                arr = [];
                obj = {};
            }
           
        })
    
        } catch(error) {
            console.log('read Error')
            resolve('write ' + linkFile + 'error')
        } 
        
    })
  
    
    
}
function   handleProcessLine (string, arr, object, type) {
    if (type == 2) {
         processLineCustom(string, arr, object)
    } else {
         processLineNormal(string, arr, object)
    }
}

function processLineCustom(string, arr, object) {
    
    let lastIndexOfNAME = string.lastIndexOf('#Name:')
    let lastIndexOfIPTVLINK = string.lastIndexOf('http') >=0 || string.lastIndexOf('https') >=0
    if (lastIndexOfNAME >=0 && lastIndexOfIPTVLINK){
    object["name"] = string.substr(lastIndexOfNAME).replace(/[^a-zA-Z0-9 ]/g, "");
    object["createDate"] = Date();
    object["updateDate"] = Date();
    object['link'] = string.substr(0, lastIndexOfNAME).trim();
    object["isLive"] = true;
    arr.push(Object.assign({},object));
    object = {};
}
}

function processLineNormal(string, arr, object) {
    let lastIndexOfEXTINF = string.lastIndexOf('#EXTINF')
    let lastIndexOfIPTVLINK = string.lastIndexOf('http') >=0 || string.lastIndexOf('https') >=0
    if (lastIndexOfEXTINF >=0){
        let lastIndexComma = string.lastIndexOf(',') 
        lastIndexComma = lastIndexComma ? lastIndexComma+1 : lastIndexOfEXTINF + 7
        object["name"] = string.substr(lastIndexComma).replace(/[^a-zA-Z0-9 ]/g, "");
        object["createDate"] = Date();
        object["updateDate"] = Date();
        object["isLive"] = true;
    }
    if (lastIndexOfIPTVLINK &&  string.lastIndexOf('="http://bit.ly') < 0 && string.lastIndexOf('.png') < 0  && string.lastIndexOf('.jpg') <0 ) {
        object['link'] = string.trim();
        //arr[arr.length] = Object.assign({}, object);
        arr.push(Object.assign({},object));
        object = {};
    } 
}

module.exports = {
    readFileM3U : readFileM3U
}