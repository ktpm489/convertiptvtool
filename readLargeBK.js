var fs  = require('fs');
var readline = require('readline');
var path = require("path");
var stream = require('stream');
var dateFormat = require('dateformat');
 // 1 -> NORMAL, 2-> Read from http://www.iptvtools.net/#
function readFileM3U(linkFile , type = 1) {
    try {
        var instream = fs.createReadStream(linkFile);
    var outstream = new stream;
    var r1 = readline.createInterface(instream, outstream);
    let arr  = [];
    let obj = {};
    r1.on('line', function(line){
        handleProcessLine(line,arr, obj,type)
    })
    r1.on('close', function(){
        try {
            var myJsonString = JSON.stringify(arr);
            console.log('close', 'myJsonString', myJsonString)
            var extension = path.extname(linkFile);
            var file = path.basename(linkFile,extension)+ '_'+dateFormat(new Date(), "dd_mm_yyyy_HH_MM_ss_TT");
            console.log('file', file )
            fs.writeFile(file, myJsonString,function(err){
                if (err){
                    return console.log('err',err);
                }
                console.log('save file OK');
            } )
        } catch (error){
            console.log('error',error)
        }finally {
            arr = [];
            obj = {};
        }
       
    })

    } catch(error) {
        console.log('read Error')
    } 
    
    
}
function handleProcessLine (string, arr, object, type) {
    if (type == 2) {
        processLineCustom(string, arr, object)
    } else {
        processLine(string, arr, object)
    }
}

function processLineCustom(string, arr, object) {
    let lastIndexOfNAME = string.lastIndexOf('#Name:')
    let lastIndexOfIPTVLINK = string.lastIndexOf('http') >=0 || string.lastIndexOf('https') >=0
    if (lastIndexOfNAME >=0 && lastIndexOfIPTVLINK){
    object["name"] = string.substr(lastIndexOfNAME).replace(/[^a-zA-Z0-9 ]/g, "");
    object["createDate"] = Date();
    object["updateDate"] = Date();
    object['link'] = string.substr(0, lastIndexOfNAME).trim();
    object["isLive"] = true;
    arr.push(Object.assign({},object));
    object = {};
}
}

function processLine(string, arr, object) {
    let lastIndexOfEXTINF = string.lastIndexOf('#EXTINF')
    let lastIndexOfIPTVLINK = string.lastIndexOf('http') >=0 || string.lastIndexOf('https') >=0
    if (lastIndexOfEXTINF >=0){
        let lastIndexComma = string.lastIndexOf(',') 
        lastIndexComma = lastIndexComma ? lastIndexComma+1 : lastIndexOfEXTINF + 7
        object["name"] = string.substr(lastIndexComma).replace(/[^a-zA-Z0-9 ]/g, "");
        object["createDate"] = Date();
        object["updateDate"] = Date();
        object["isLive"] = true;
    }
    if (lastIndexOfIPTVLINK) {
        object['link'] = string.trim();
        arr.push(Object.assign({},object));
        object = {};
    } 
}
