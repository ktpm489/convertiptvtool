const request = require('request')
const cheerio = require('cheerio')
const fs = require('fs')
const fetch = require('node-fetch')

function crawlerData() {
    request('https://radiowebsites.org', function (err, res, body) 
    {
        var $ = cheerio.load(body)
        var list = [];
        $('ul[class="country-list"]').find('li > a').each(function (index, element) {
            list.push({ 'name' :  $(element).text(), 'link' : $(element).attr('href') });
            });
        console.dir(list);
    })
}

// https://stackoverflow.com/questions/37576685/using-async-await-with-a-foreach-loop
function crawlerDataIPTV() {
    request('https://freeiptv.io/playlists/?c=FR', function (err, res, body) 
    {
        var $ = cheerio.load(body)
        var list = [];
        $('select[id="select_country"]').find('option').each(function (index, element) {
           // list.push({ 'name' :  $(element).text(), 'link' : $(element).attr('href') });
                list.push({ 'name' :  $(element).text(), 'link':'https://freeiptv.io/m3u/v527m_normal.php?c=' + $(element).attr('value') });
            });
         console.dir(list);
        // list.forEach(function(e){
        //   fetchAndSaveData(e)
        // })
    })
}

function fetchAndSaveData(linkUrl) {
    fetch(linkUrl)
    .then(res => res.text())
    .then(body => {
      //  var strName = linkUrl.substring(linkUrl.lastIndexOf("https://freeiptv.io/m3u/v527m_normal.php?c="))
        console.log(body)
    });
}
crawlerDataIPTV();