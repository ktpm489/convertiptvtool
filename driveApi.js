var { google } = require('googleapis');
var drive = google.drive("v3");
// import private key
var key = require('./personal.json');
// import path directories calls
var path = require("path");
var fs = require("fs")
var { getAllFilesInGoogleFolder } = require('./listFile.js')
var { uploadGoogleDriveFile } = require('./uploadFile.js')
// make the request to  retrievean authorization  to work  with the google drive web
var jwToken = new google.auth.JWT(
    key.client_email,
    null,
    key.private_key, ["https://www.googleapis.com/auth/drive"],
    null
);
jwToken.authorize((authErr) => {
    if (authErr) {
        console.log("error :" + authErr);
    } else {
        console.log("Authorization accorded");
    }

})

// getAllFilesInGoogleFolder(drive,"1vyzGqOTwIxYFMDdKfFvgPcvLYmmTF4WX",jwToken);
uploadGoogleDriveFile("1vyzGqOTwIxYFMDdKfFvgPcvLYmmTF4WX", 'text.txt', drive,jwToken);